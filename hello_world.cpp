// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  mylib::helloWorld( "... anywho ..." );

  mylib::List list{2,3,4};

//TESTING
  int arr[4]={-40,30,20,-10};

  for (int i = 0; i < 3; i++)
  {
      //int i = 0;

      int j = i + 1;
      int k = i;
      for (; j>=1 ;--j)
      {
          if (arr[j]<arr[k])
          {
              int temp = 0;
              temp = arr[k];
              arr[k] = arr[j];
              arr[j] = temp;
          }
          --k;
      }
  }

  //display
  for (int i = 0; i<4; i++)
  {
      std::cout << arr[i] << " ";
  }
  std::cout<< std::endl;

//END OF TESTING

  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
