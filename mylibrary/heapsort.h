#ifndef HEAPSORT
#define HEAPSORT
#include <functional>
#include <cmath>
#include <vector>
#include <iterator>
#include <algorithm>

namespace mylib {

template <typename Iterator>
void heap_sort(Iterator first, Iterator last)
{
    std::make_heap(first, last);
    std::sort_heap(first, last);
}

}
#endif // HEAPSORT

