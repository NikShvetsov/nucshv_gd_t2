#ifndef INTROSORT
#define INTROSORT

#include <functional>
#include <cmath>
#include <vector>
#include <iterator>
#include <algorithm>
#include "shellsort.h"
#include "selection_sort.h"
#include "heapsort.h"

namespace mylib {

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void Insertion_sort ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() )
{
    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    for( size_type i {0}; i < n-1; ++i )
    {
        size_type j {i + 1};
        size_type k {i};
        for (; j>=1 ;--j)
        {
            if (cmp(first[j],first[k]))
            {
                std::swap(first[k],first[j]);
            }
            --k;
        }
    }
}

namespace privateIntroloop{

template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
void Introsort_loop ( RandomAcessIterator first, RandomAcessIterator last, typename RandomAcessIterator::difference_type depth_limit)
{
    using size_type = typename RandomAcessIterator::difference_type;
    //size_type n = last - first;
    //int depth_limit = floor_lg;
    //double treshhold = log(n);
    //int size_treshhold {(last-first)/2};
    size_type size_treshhold = 16; // Threshold for the size of the evaluation sequence
    //best value by statistics
    //Determine the size of the sequence, if it is less than or
    //equal to 16 Sort Heap Sort, left Insertion Sort the final completion of the sort

    while ( last - first > size_treshhold)
    {
        if (depth_limit == 0)
        {
            Insertion_sort(first,last);
            return;
        }
        else
        {
            --depth_limit;
            RandomAcessIterator cut {first +(last-first)/2};
            Introsort_loop(cut,last, depth_limit);
            last = cut;
        }
    }
    //shellSort(first,last);
  }
}

template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
void Introsort ( RandomAcessIterator first, RandomAcessIterator last )
{
    using size_type = typename RandomAcessIterator::difference_type;
    size_type depth_limit = (2 * std::floor(std::log2(last-first))); //floor value
    privateIntroloop::Introsort_loop(first,last,depth_limit); //preparation by Introsort_loop
    selection_sort(first,last);
    //may use heap_sort - results are STL-like
}

//template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
//void Insertion_sort2 ( RandomAccessIterator first, RandomAccessIterator last, Compare cmp = Compare() )
//{
//  //using size_type = typename RandomAccessIterator::difference_type;
//  //size_type n {last - first};
//  RandomAccessIterator min = first, j , i;

//      for( i = first + 1; i < last; ++i )
//      {
//          if ( *i < *min )
//          {
//              min = i;
//          }
//      }

//      std::iter_swap( first, min );

//      while( ++first < last )
//      {
//          for( j = first; *j < *(j - 1); --j )
//          {
//              std::iter_swap( (j - 1), j );
//          }
//      }
//}
}
#endif // INTROSORT

