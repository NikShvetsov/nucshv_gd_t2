% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

% Math
\usepackage{amsmath}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}

\lstset{language = C++,
		breaklines = true,
		frame = single}

% Set TITLE, AUTHOR and DATE
\title{Report: Introspective Sorting Algorithm Implementation and It's Comparison}
\author{Shvetsov Nikita}
\date{\today}
 


\begin{document}

  % Create the main title section
  \maketitle

  \begin{abstract}
    Introsort or introspective sort is a hybrid sorting algorithm that provides both fast average 
	performance and (asymptotically) optimal worst-case performance. In standard implementation it 
	begins with insertion sorting and switches to heapsort when the recursion depth exceeds a level 
	based on (the logarithm of) the number of elements being sorted. 
	This combines the good parts of both algorithms, with practical performance comparable to 
	insertion sorting on typical data sets and worst-case $\Theta$($N log(N)$) runtime due to the heap sort. 
	In this situation we used 'Shellsort' and 'Selection sort' algorithm for experimenting with generated data.
	Given an array of size N, sorting can be done in $\Theta$($N log(N)$) in average. 
	In this report implementation of changed Introsort algorithm is given and comparison analysis of 
	reviewed algorithms.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  \section{Introduction}
	A sorting algorithm is an algorithm that puts elements of a list in a certain order. Efficient 
	sorting is important for optimizing the use of other algorithms which require input data to be in sorted lists.
	The most frequently used sorting algorithms are insertion sorting, heapsort and mergesort. 
	They usually require $\Theta$($log(N)$), $\Theta$(1) and $\Theta$($N$) working space, respectively~\cite{datastr:2009}.
	There are a lot of other algorithms that can be easily implemented, but require more time to sort data.
	Insertion sort is the preferred in-place sorting algorithm in many contexts, since its average 
	computing time on uniformly distributed inputs is $\Theta$($N log N$) and it is in fact faster than most 
	other sorting algorithms on most inputs. Its drawback is that its worst-case time bound is $\Theta$($N$).
	A similar dilemma exists with selection algorithms (for finding the i-th largest element) 
	based on partitioning~\cite{musser:1974}.
	So usage of algorithm combination, depending on parameters of data sets is required.
  \section{Selection Sort}
  Selection sort is a sorting algorithm, specifically an in-place comparison sort. It has $\Theta$($N^2$) 
  time complexity, making it inefficient on large lists, and generally performs worse than the similar 
  insertion sort. Selection sort is noted for its simplicity, and it has performance advantages over more 
  complicated algorithms in certain situations, particularly where auxiliary memory is limited. The algorithm 
  divides the input list into two parts: the sublist of items already sorted, which is built up from left to 
  right at the front (left) of the list, and the sublist of items remaining to be sorted that occupy the rest 
  of the list. Initially, the sorted sublist is empty and the unsorted sublist is the entire input list. 
  The algorithm proceeds by finding the smallest (or largest, depending on sorting order) element in the 
  unsorted sublist, exchanging it with the leftmost unsorted element (putting it in sorted order), and 
  moving the sublist boundaries one element to the right. The following sorting algorithm is used in 
  implementation of Introsort algorithm.
  
  The following pseudo code (see $Algorithm 1$.) shows the algorithm of Selection sort~\cite{shell:1959}.
  \begin{algorithm}
    \caption{Selection sort algorithm}\label{euclid}
    \begin{algorithmic}[1]
      \Procedure{SelectionSort}{$first,last$}%\Comment{The g.c.d. of a and b}
      \State $n\gets last - fist$
	  \State for each element i in data \{
      \State j = i
      \State k = j+1 then
	  \State for each k in data \{ 
	  \State if first{$ k $} equals first{$ j $} then j equals k;
      \State if i not equal j then swap values of first{$ i $} and first{$ j $}; 
	  \State \}\}
      \EndProcedure
    \end{algorithmic}
  \end{algorithm}
  
  \section{Introsort}
  Usually Introsort algorithm is implemented with Insertion and Heap sort algorithm.
  Due to the special variable (size threshold) algorithm switches between those sort 
  algorithms depending on input data~\cite{shell:1959}. In our case we used Selection sort instead of Heapsort
  algorithm. This change can severely influence on big data sets. The threshold value is chosen 
  by statistics of various results and equals 16. As with typical heapsort implementations 
  (e.g. The June 2000 SGI C++ STL introsort implementation), delegate to insertion sort for ranges of size below 16.
  
  The following source code shows the algorithm of Introsort.

  \begin{lstlisting}
    
namespace privateIntroloop{

template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
void Introsort_loop ( RandomAcessIterator first, RandomAcessIterator last, typename RandomAcessIterator::difference_type depth_limit)
{
    using size_type = typename RandomAcessIterator::difference_type;
    size_type size_treshhold = 16; // Threshold for the size of the evaluation sequence
    //best value by statistics
    //Determine the size of the sequence, if it is less than or
    //equal to 16 Sort Heap Sort, left Insertion Sort the final completion of the sort

    while ( last - first > size_treshhold)
    {
        if (depth_limit == 0)
        {
            Insertion_sort(first,last);
            return;
        }
        else
        {
            --depth_limit;
            RandomAcessIterator cut {first +(last-first)/2};
            Introsort_loop(cut,last, depth_limit);
            last = cut;
        }
    }
  }}

template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
void Introsort ( RandomAcessIterator first, RandomAcessIterator last )
{
    using size_type = typename RandomAcessIterator::difference_type;
    size_type depth_limit = (2 * std::floor(std::log2(last-first))); //floor value
    privateIntroloop::Introsort_loop(first,last,depth_limit); //preparation by Introsort_loop
    selection_sort(first,last);
    //may use heap_sort - results are STL-like
}
  \end{lstlisting}
  

  \section{Benchmark set-up}
  For the data construction benchmark was used. It counts how much time it takes for a set of 
  data to be sorted by all used algorithms in the project. It uses a pair like (data size - sorting time). 
  Overall 9 sets were used, elements from 20 till 100000.
  %Describe how the benchmark was performed. Be objective - don't describe too many details.
  %The reader which this text is meant for should be able to reconstruct the experiment using his own tools.
  %Some examples of topics for this section:
  
  All tests were performed on a computer with the following specifications:
	
	\begin{itemize}
      \item CPU (AMD Dual-Core E201800);
      \item RAM (DDRIII 2GB).
	\end{itemize}
  For the sorting algorithms, a data set of $N$ integers were sorted, where duration 
  times were measured by using the high precision chrono device which is included with the STL.
    

  \section{Results}
	STL sort, Bubble sort, Shell sort, Selection sort and Introsort algorithms were analyzed (see \cref{fig:bench_sort})
	
	The X-axis represent data size, Y-axis - time in ms. The plot shows that STL sort is a 
	high performance algorithm, which sorts all sets of presented data with nearly equal speed.
	
	Introsort and Selection sorting algorithms shows similar (in certain conditions) results, slower than STL sort, but 
	faster than Bubble and Shell sort.
    \begin{figure}[H]%[width=\textwidth]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=0.9\textwidth, height=0.25\textheight,
%          reverse legend
          ]

          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_bubble.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_shell.dat};
          \addplot[black] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_selection_sort.dat};
            \addplot[yellow] table[x=size ,y=time, skip first n=0] {dat/benchmark_introsort.dat};
          \legend{
            STL sort,
            Bubble,
            Shell,
            Selection,
			Introsort
          }
        \end{axis}
      \end{tikzpicture}
      \caption{Benchmark results for sorting algorithms}
      \label{fig:bench_sort}
    \end{figure}

  \section{Concluding remarks}
  \begin{itemize}
    %\item Reflect over the method and results.
	\item The fastest sorting algorithm, according to the tests is STL sort. 
	The plot shows that implemented method is far behind the STL sort algorithm, 
	though it have better results than Bubble, Shell and Selection sorting algorithms. 
	The effectiveness is rather close to Selection sort algorithm.
    \item For future plans, other implementation should be reviewed for better results.
	For example, testing the original implementation of Introspective sort algorithm: 
	using of Heap (non-STL) and Insertion (that may be improved) sorting algorithms. Also Partition function should be redefined.
  \end{itemize}
  By comparing the performance of the sorting algorithms from \emph{mylib} and STL we conclude that the STL algorithms is 
  greatly faster at sorting. To achieve these results we should improve sorting algorithm for Introsort to have close results in further experiments.


  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
